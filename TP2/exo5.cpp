#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

Array& merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)

	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split

    for (int i = 0; i <= first.size()-1; i++)
        first[i] = origin[i];

    for (int i = 0; i <= second.size()-1; i++)
        second[i] = origin[first.size()+i];

	// recursiv splitAndMerge of lowerArray and greaterArray

    if (first.size() > 1)
        splitAndMerge(first);

    if (second.size() > 1)
        splitAndMerge(second);

    // merge

    origin = merge(first, second, origin);

    return;
}

Array& merge(Array& first, Array& second, Array& result)
{
    if ((first[0] < second[0]) || (first[0] == second[0])) {
        result[0] = first[0];
        result[1] = second[0];
    } else {
        result[0] = second[0];
        result[1] = first[0];
    }

    return result;
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
