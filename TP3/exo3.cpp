#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>
#include <QDebug>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left;
    Node* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children

        this->value = value;
        this->left = nullptr;
        this->right = nullptr;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child

        // insère un nouveau noeud dans l'arbre avec la valeur value

        if (value <= this->value) {
            if (this->left == nullptr) {
                this->left = new BinarySearchTree(value);
            } else {
                this->left->insertNumber(value);
            }
        } else {
            if (this->right == nullptr) {
                this->right = new BinarySearchTree(value);
            } else {
                this->right->insertNumber(value);
            }
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        // retourne la hauteur de l'arbre

        if (this->left == nullptr)
            return 0;
        if (this->right == nullptr)
            return 0;

        int hg = this->left->height();
        int hd = this->right->height();

        if(hg > hd)
            return 1+hg;
        else
            return 1+hd;
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        // retourne le nombre de noeuds de l'arbre

        return 1;
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)

        // retourne vrai si le noeud est une feuille, retourne faux sinon

        if (this->left == nullptr && this->right == nullptr)
            return true;
        else
            return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if (this->left != nullptr && this->right != nullptr) {
            this->left->inorderTravel(nodes, nodesCount);
            nodes[nodesCount] = this;
            nodesCount += 1;
            this->right->inorderTravel(nodes, nodesCount);
        }
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        if (this->left != nullptr && this->right != nullptr) {
            nodes[nodesCount] = this;
            nodesCount += 1;
            this->left->preorderTravel(nodes, nodesCount);
            this->right->preorderTravel(nodes, nodesCount);
        }

	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if (this->left != nullptr && this->right != nullptr) {
            this->left->preorderTravel(nodes, nodesCount);
            this->right->preorderTravel(nodes, nodesCount);
            nodes[nodesCount] = this;
            nodesCount += 1;
        }
	}

	Node* find(int value) {
        // find the node containing value

        if (this->value == value) {
            return this;
        } else {
            if (value < this->value) {
                return this->left->find(value);
            } else {
                return this->right->find(value);
            }
        }

		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 10;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
