#include <QApplication>
#include <time.h>
using namespace std;

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){

    // L'algorithme par "force brute" pour trier un tableau est le tri par sélection. Le principe est tout simplement de chercher à chaque fois
    // l'élément le plus petit du tableau pour le placer au début.

    int emplacement = 0;
    int minimum = toSort[0];
    for (int caseCourante = 0; caseCourante <= toSort.size()-1; caseCourante++) {
        minimum = toSort[caseCourante];
        emplacement = caseCourante;
        for (int i = caseCourante+1; i <= toSort.size()-1; i++) {
            if (minimum > toSort[i]) {
                minimum = toSort[i];
                emplacement = i;
                toSort[emplacement] = toSort[caseCourante];
                toSort[caseCourante] = minimum;
            }
        }
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
