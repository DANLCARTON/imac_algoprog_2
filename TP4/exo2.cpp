#include <time.h>
#include <stdio.h>

#include <QApplication>
#include <QDebug>

#include "tp3.h"
#include "HuffmanNode.h"

_TestMainWindow* w = nullptr;
using std::size_t;
using std::string;

void HuffmanHeap::insertHeapNode(int heapSize, unsigned char c, int frequences)
{
    int i = heapSize;
    (*this)[i] = HuffmanNode(c, frequences);
    while (i > 0 && (*this)[i].frequences > (*this)[(i-1)/2].frequences) {
        (*this).swap(i, (i-1)/2);
        i = (i-1)/2;
    }
}

void HuffmanNode::insertNode(HuffmanNode* node)
{
    if( this->isLeaf() )
    {
        HuffmanNode* copy = new HuffmanNode(this->character, this->frequences);
        if (node->frequences*2 < (*this).frequences) {
            (*this).right = copy;
            (*this).left = node;
        } else {
            (*this).right = node;
            (*this).left = copy;
        }
        (*this).character = '\0';
        (*this).frequences = (*this).right->frequences+(*this).left->frequences;
    }
    else
    {
        if ((*this).frequences > node->frequences*3) {
            (*this).left->insertNode(node);
        } else {
            (*this).right->insertNode(node);
        }
    }
    this->frequences += node->frequences;
}

void HuffmanNode::processCodes(std::string baseCode)
{
    if ((*this).isLeaf()) {
        (*this).code = baseCode;
    } else {
        if ((*this).left) {
            (*this).left->processCodes(baseCode + '0');
        }
        if ((*this).right) {
            (*this).right->processCodes(baseCode + '1');
        }
    }
}

void HuffmanNode::fillCharactersArray(HuffmanNode** nodes_for_chars)
{
    if (!this->left && !this->right)
        nodes_for_chars[this->character] = this;
    else {
        if (this->left)
            this->left->fillCharactersArray(nodes_for_chars);
        if (this->right)
            this->right->fillCharactersArray(nodes_for_chars);
    }
}

void charFrequences(string data, Array& frequences)
{
    for (int i = 0; i < data.size(); i++) {
        frequences[data[i]]++;
    }
}

void huffmanHeap(Array& frequences, HuffmanHeap& heap, int& heapSize)
{
    heapSize = 0;
    for (size_t i = 0; i < frequences.size(); i++) {
        if (frequences[i] != 0) {
            heap.insertHeapNode(heapSize, (char)i, frequences[i]);
            heapSize++;
        }
    }
}

void huffmanDict(HuffmanHeap& heap, int heapSize, HuffmanNode*& dict)
{
    dict = new HuffmanNode(heap[0].character, heap[0].frequences);
    for (int i = 1; i < heapSize; i++) {
        HuffmanNode* temp = new HuffmanNode(heap[i].character, heap[i].frequences);
        dict->insertNode(temp);
    }
}

string huffmanEncode(HuffmanNode** characters, string toEncode)
{
    string encoded = "";
    for (size_t i = 0; i < toEncode.size(); i++) {
        encoded = encoded.append(characters[(int)toEncode[i]]->code);
    }
    return encoded;
}

string huffmanDecode(HuffmanNode* dict, string toDecode)
{
    string decoded = "";
    HuffmanNode* temp = dict;
    for (size_t i = 0; i < toDecode.size(); i++) {
        if (temp->isLeaf()) {
            decoded += temp->character;
            temp = dict;
        }
        if (toDecode[i] == '0') {
            temp = temp->left;
        } else if (toDecode[i] == '1') {
            temp = temp->right;
        }
    }
    decoded += temp->character;
    return decoded;
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Array::wait_for_operations = false;
    w = new _TestMainWindow();

    string data = "Wesh, bien ou bien ? Ceci est une chaine de caracteres sans grand interet. J'ai envie de me foutre en l'air putain.";

    Array& frequences = w->newArray(256);
    HuffmanHeap heap(256);
    HuffmanNode* dict;
    int i;

    for (i=0; i < (int)frequences.size(); ++i)
        frequences.__set__(i, 0);

    charFrequences(data, frequences);

    for (i=0; i < (int)frequences.size(); ++i)
        if (frequences[i]>0)
            qDebug() << (char)i << ": " << frequences[i];

    int heapSize=0;

    huffmanHeap(frequences, heap, heapSize);
    huffmanDict(heap, heapSize, dict);
    dict->processCodes("");

    HuffmanNode* characters[256];
    memset(characters, 0, 256 * sizeof (HuffmanNode*));
    dict->fillCharactersArray(characters);

    string encoded = huffmanEncode(characters, data);
    string decoded = huffmanDecode(dict, encoded);

    w->addBinaryNode(dict);
    w->updateScene();
    qDebug("Encoded: %s\n", encoded.c_str());
    qDebug("Decoded: %s\n", decoded.c_str());
    w->show();

    return a.exec();
}
