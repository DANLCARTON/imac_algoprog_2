#include <QApplication>
#include <time.h>
#include <qDebug>

#include "tp2.h"



MainWindow* w=nullptr;



void recursivQuickSort(Array& toSort, int size)
{

    // Le tri rapide se base sur le principe de "diviser pour reigner". Le but est de diviser le tableau en deux parties, les petites et les grandes
    // valeurs, puis de réitérer jusqu'à n'avoir qu'un seul élément par tableau. En fusionnant les tableaux on obtient un tableau trié.

    // t ← tableau de nombres aléatoires
    // pivot ← un nombre quelconque de t
    // lowers ← tableau contenant les nombres plus petits que t
    // greaters ← tableau contenant les nombres plus grands que t
    // trier lowers et greaters
    // fusionner lower, pivot et greaters

    // stop statement = condition + return (return stop the function even if it does not return anything)

    qInfo() << "size = " << size;
	
    Array& pivotArray= w->newArray(1);
    Array& lowerArray = w->newArray(size-1);
    Array& greaterArray= w->newArray(size-1);
    int lowerSize = 0, greaterSize = 0; // effectives sizes

    // split

    if (size > 1) {
        pivotArray[0] = toSort[0];
        for (int i = 0; i <= size-1; i++) {
            if (toSort[i] < pivotArray[0]) {
                lowerArray.insert(lowerSize, toSort[i]);
                lowerSize++;
            } else if (toSort[i] > pivotArray[0]) {
                greaterArray.insert(greaterSize, toSort[i]);
                greaterSize++;
            }
        }
	
        // recursiv sort of lowerArray and greaterArray

        if (lowerSize > 1)
            recursivQuickSort(lowerArray, lowerSize);

        if (greaterSize > 1)
            recursivQuickSort(greaterArray, greaterSize);

        // merge

        // j'arrive définitivement pas à merge
    }
}

void quickSort(Array& toSort){
    recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=10;
    MainWindow::instruction_duration = 60;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
