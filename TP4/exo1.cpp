#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

#include <QDebug>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex) // i*2+1
{
    int iG = nodeIndex*2+1;

    return iG;
}

int Heap::rightChild(int nodeIndex) // i*2+2
{
    int iD = nodeIndex*2+2;

    return iD;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i

    qDebug() << "heapSize " << heapSize;
    qDebug() << "value " << value;

    int i = heapSize;
    (*this)[i] = value;
    while (i > 0 && (*this)[i] > (*this)[(i-1)/2]) {
        (*this).swap(i, (i-1)/2);
        i = (i-1)/2;
    }

    // (*this)[heapSize] = value;
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i

    qDebug() << "nodeIndex " << nodeIndex;
    qDebug() << "heapSize " << heapSize;

    int i = nodeIndex;
    int i_max = nodeIndex; // je définis i et i_max l'index du noeud que je regarde

    qDebug() << "this[i = " << (*this)[i];

    if (this->leftChild(i) < heapSize && this->rightChild(i) < heapSize) { // si l'index des enfants sont inférieurs à la taille du tas
        if ((*this)[i] < (*this)[this->leftChild(i)] || (*this)[i] < (*this)[this->rightChild(i)]) { //si la valeur du noeud est inférieure à l'enfant gauche
            if ((*this)[this->leftChild(i)] > (*this)[this->rightChild(i)]) {
                i_max = leftChild(i);
            } else {
                i_max = rightChild(i);
            }
        }
    }

    qDebug() << i;
    qDebug() << i_max;

    if (i_max != i) {
        (*this).swap(i, i_max);
        heapify(heapSize, i_max);
    }
}

void Heap::buildHeap(Array& numbers)
{
    qDebug() << "tu te lances là ?";
    // non
}

void Heap::heapSort()
{
    int n = this->size();
    for (int i = n-1; i > 0; i--) {
        this->swap(0, i);
        this->heapify(i, 0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 10;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
