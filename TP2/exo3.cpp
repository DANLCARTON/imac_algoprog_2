#include <QApplication>
#include <time.h>
#include <qDebug>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){

    // Le tri a bulles consiste à tester chaque paires de cases adjacentes et de faire remonter les valeurs les plus grandes comme des bulles d'air
    // dans l'eau

    // t ← tableau de nombres aléatoires
    // pour chaque indice i de t faire:
    // | pour chaques cases adjacentes faire:
    // | | remonter la valeur la plus haute

    bool yAEuUnEchange = false;

    for (int i = 0; i <= toSort.size()-1; i++) {

        if (i+1 == toSort.size()) {
            i = -1;
            yAEuUnEchange = false;
        } else if (toSort[i] > toSort[i+1]) {
            toSort.swap(i, i+1);
            yAEuUnEchange = true;
        }

        if(i+2 == toSort.size() && !yAEuUnEchange) {
            break;
        }
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
    MainWindow::instruction_duration = 60;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
