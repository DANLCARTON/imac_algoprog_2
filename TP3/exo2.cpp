#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <QDebug>

MainWindow* w = nullptr;
using std::size_t;

/**
 * @brief define indexMin and indexMax as the first and the last index of toSearch
 * @param array array of int to process
 * @param toSearch value to find
 * @param indexMin first index of the value to find
 * @param indexMax last index of the value to find
 */

void bubbleSort(Array& toSort){

    // Le tri a bulles consiste à tester chaque paires de cases adjacentes et de faire remonter les valeurs les plus grandes comme des bulles d'air
    // dans l'eau

    // t ← tableau de nombres aléatoires
    // pour chaque indice i de t faire:
    // | pour chaques cases adjacentes faire:
    // | | remonter la valeur la plus haute

    bool yAEuUnEchange = false;

    for (int i = 0; i <= toSort.size()-1; i++) {

        if (i+1 == toSort.size()) {
            i = -1;
            yAEuUnEchange = false;
        } else if (toSort[i] > toSort[i+1]) {
            toSort.swap(i, i+1);
            yAEuUnEchange = true;
        }

        if(i+2 == toSort.size() && !yAEuUnEchange) {
            break;
        }
    }
}

int binarySearch(Array& array, int toSearch)
{

    // retourne l'index de la valeur toSearch ou -1 si la valeur n'est pas dans le tableau

    int start = 0;
    int end = array.size();
    int mid;
    int foundIndex = -1;

    while (start < end) {
        mid = (start+end)/2;
        if (toSearch > array[mid])
            start = mid+1;
        else if (toSearch < array[mid])
            end = mid;
        else {
            foundIndex = mid;
            break;
        }
    }

    return foundIndex;
}

void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax)
{
    // remplit l'index minimum et maximum de la valeur toSearch. Si la valeur n'est pas dans le tableau, remplit les deux index par -1.

    // do not use increments, use two different binary search loop

    Array indexes(array.size());
    Array arrayArray = array;

    int i = 0;

    while (true) {
        qDebug() << "while - - - ";
        for (int j = 0; j <= indexes.size()-1; j++)
            qDebug() << indexes[j];
        indexes[i] = binarySearch(arrayArray, toSearch);
        if (indexes[i] != -1) {
            arrayArray[indexes[i]] = -1;
        } else if (indexes[i] == -1) {
            bubbleSort(indexes);
            for (int j = 0; j <= indexes.size()-1; j++)
            if (indexes[indexes.size()-1] == -1) {
                indexMin = indexMax = -1;
            } else if (indexes[indexes.size()-2] == -1) {
                indexMin = indexMax = indexes[indexes.size()-1];
            } else {
                indexMax = indexes[indexes.size()-1];
                for (int j = 0; j <= indexes.size()-1; j++) {
                    if (indexes[j] != -1) {
                        indexes.swap(0, j-1);
                    }
                }
                indexMin = indexes[0];
            }
            break;
        }
        i++;
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 10;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}
