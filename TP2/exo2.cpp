#include <QApplication>
#include <time.h>
#include <QTextStream>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){

    // Une autre version de la "force brute" est le tri par insertion. Le tri consiste à déplacer les valeurs d'un tableau à un autre en déterminant
    // à chaque fois où doit se positionner le nombre par rapport aux nombres déjà présents dans le deuxième tableau.

    // t ← tableau de nombres aléatoires
    // result ← tableau vide
    // result[0] ← t[0]
    // pour chaque nombre n de t (exepté le premier) faire:
    // | si pour tout m appartenant à result tel que m > n alors:
    // | | insérer n à la position de m (en décalant le reste du tableau)
    // | sinon:
    // | | insérer n à la fin

    Array& sorted=w->newArray(toSort.size());

    sorted[0] = toSort[0];
    bool TRIE = false;

    for (int i = 1; i <= toSort.size()-1; i++) {
        for (int m = 0; m <= toSort.size()-1; m++) {
            TRIE = false;
            if (sorted[m] > toSort[i]) {
                sorted.insert(m, toSort[i]);
                TRIE = true;
            }
            if (TRIE) {
                break;
            }
        }
        if (!TRIE) {
            sorted[i] = toSort[i];
        }
    }

	// insertion sort from toSort to sorted
	
    toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=10; // number of elements to sort
    MainWindow::instruction_duration = 400; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
