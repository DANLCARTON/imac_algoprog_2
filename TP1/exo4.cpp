#include "tp1.h"
#include <QApplication>
#include <time.h>

#define return_and_display(result) return _.store(result);

void allEvens(Array& evens, Array& array, int evenSize, int arraySize)
{ // replit evens avec tous les nombres pairs de array. evenSize represente le nombre de valeurs dans evens (est donc egale à 0 au début) et arraySize est le nombre de valeurs dans array.
    Context _("allEvens", evenSize, arraySize); // do not care about this, it allow the display of call stack

    // your code
    int evenIndex = 0;
    for (int i = 0; i <= arraySize-1; i++) {
        if (array[i]%2 == 0) {
            evens[evenIndex] = array[i];
            evenIndex++;
        }
    }

    return;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);  // create a window manager
    MainWindow::instruction_duration = 400;  // make a pause between instruction display
    MainWindow* w = new AllEvensWindow(allEvens); // create a window for this exercice
    w->show(); // show exercice

    return a.exec(); // main loop while window is opened
}




