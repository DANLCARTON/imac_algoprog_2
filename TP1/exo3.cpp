#include "tp1.h"
#include <QApplication>
#include <time.h>

#define return_and_display(result) return _.store(result);


int search(int value, Array& toSort, int size)
{ // retourne l'index de value dans array
    Context _("search", value, size); // do not care about this, it allow the display of call stack

    // your code
    // [xx, xx, xx, xx, xx, xx, xx, xx]

    for (int i = 0; i <= size; i++) {
        if (value == toSort[i]) {
            return_and_display(i);
        }
    }

}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);  // create a window manager
    MainWindow::instruction_duration = 400;  // make a pause between instruction display
    MainWindow* w = new SearchWindow(search); // create a window for this exercice
    w->show(); // show exercice

    return a.exec(); // main loop while window is opened
}




