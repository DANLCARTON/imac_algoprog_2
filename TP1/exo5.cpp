#include "tp1.h"
#include <QApplication>
#include <time.h>

long power(long value, long n)
{
    int powered = value;

    for (int i = 0; i <= n-2; i++) {
        powered = powered*value;
    }
    return powered;
}

int isMandelbrot(Point z, int n, Point point){
    // retourne vrai si le point appartient à l'ensemble de Mandelbrot pour la fonction f(z)→z²+point. Un point appartient à cet ensemble si zn est
    // bornée, autrement dit s'il existe un i < n tel que |zi| < 2.
    // recursiv Mandelbrot


    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



